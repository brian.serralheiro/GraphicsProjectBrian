﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperJump : MonoBehaviour
{
    private bool flag;
    //Audio
    [SerializeField]
    private AudioClip SuperJumpSFX;
    [SerializeField]
    private AudioSource audioSource;

    void Update()
    {
        if(flag && transform.position.z < -8)
        {
            flag = false;
            transform.position = new Vector3(Random.Range(-1, 2), transform.position.y, transform.position.z);
        }
        //It only going to change the position once per spawn
        if(transform.position.z > 10)
        {
            flag = true;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Speedster>())
        {
            playSound(SuperJumpSFX);
            other.gameObject.GetComponent<Speedster>().Jump(1.8f);
        }
    }

    void playSound(AudioClip clip, float volume = 1.0f)
    {
        audioSource.volume = volume;
        audioSource.clip = clip;
        audioSource.Play();
    }
}
