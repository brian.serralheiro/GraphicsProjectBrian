﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData; //To use Constants script

public class WallObstacles : MonoBehaviour
{
    public static int WallGap;
	
	// Update is called once per frame
	void Update ()
    {
        // Calculating the distance from the next wall, based on walked distance.
        int i = Mathf.RoundToInt(Speedster.Distance / WallGap) * WallGap;
        //move the wall based on the distance calculated beforehand
        transform.position = new Vector3(transform.position.x, transform.position.y, i - Speedster.Distance);
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Speedster>())
        {
            other.gameObject.GetComponent<Speedster>().loselife();
        }
    }
}
