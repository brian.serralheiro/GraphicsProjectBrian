﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    public Text highestDistanceUI;
    private static float maxDistance;

	// Use this for initialization
	void Start ()
    {
        if (maxDistance < Speedster.Distance) maxDistance = Speedster.Distance;
        //Reset speedster distance, after gameover.
        Speedster.Distance = 0;
        highestDistanceUI.text = "Highest Distance: " + maxDistance;
	}

    public void MainMenu()
    {
        SceneManager.LoadScene("MyMainMenu");
    }

}
