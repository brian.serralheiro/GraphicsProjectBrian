﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{   
    // Update is called once per frame
    void Update ()
    {
        //Obstacles will move in the opposite direction of the player.
        transform.Translate(0, 0, -Speedster.SpeedPower * Time.deltaTime);

        if(transform.position.z < -10)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Speedster>())
        {
            Speedster.gainCoin(1);
            Destroy(gameObject);
        }
        if (other.gameObject.GetComponent<Obstacles>())
        {
            Destroy(gameObject);
        }
    }
}
