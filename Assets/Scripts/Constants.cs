﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameData
{
	public static class Constants
    {
        private static Dictionary<string, int> intConstants;

        private static Dictionary<string, float> floatConstants;

        public static string FilePath
        {
            get
            {
                string persistentPath = Application.persistentDataPath;
                string savePath = "/Constants.csv";

                //Return folder path in the project
                return persistentPath + savePath;
            }
        }

        public static void Initialize()
        {
            intConstants = new Dictionary<string, int>();

            floatConstants = new Dictionary<string, float>();

            string text = "";

            if(Downloader.Constant.isDone)
            {
                text = Downloader.Constant.text;
                Downloader.Constant.Dispose();
            }
            else
            {
                text = Resources.Load<TextAsset>("Constants").text;
            }

			string[] lines = text.Split('\n');

			foreach(string l in lines)
            {
				string[] data = l.Split(',');

				if(data.Length == 3)
                {
					string key = data[0];
					string type = data[1];
					string value = data[2];

					if(type == "INT")
                    {
						int v;

						if(int.TryParse(value,out v))
                        {
							intConstants.Add(key,v);
						}
                        else
                        {
                            Debug.Log(key + " not an int");
							//Handle value is not an int
						}
					}
                    else if(type == "FLOAT")
                    {
                        float f;
                        if(float.TryParse(value, out f))
                        {
                            floatConstants.Add(key, f);
                        }
                        else
                        {
                            Debug.Log(key + " not a float");
                        }
                    }
				}
			}
		}

		public static int GetInt(string key)
        {
			return intConstants[key];
		}

        public static float GetFloat(string key)
        {
            return floatConstants[key];
        }

    }
}

