﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeMenu : MonoBehaviour
{
    private static LifeMenu menu;
    private static float Timer;
    private static float Speed;
    public Text LifeUI;

	// Use this for initialization
	void Start ()
    {
        menu = this;
        gameObject.SetActive(false);
	}

    void Update()
    {
        Timer -= Time.deltaTime;
        if (Timer < 0) gameObject.SetActive(false);
    }

    public static void PAUSE()
    {
        menu.gameObject.SetActive(true);
        
    }

    public void OnEnable()
    {
        Speed = Speedster.SpeedPower;
        Speedster.SpeedPower = 0;
        Timer = 3;
        LifeUI.text = "Remaining Lives: " + Speedster.Lives;
    }

    public void OnDisable()
    {
        Speedster.SpeedPower = Speed;
        //Round the distance downward to make the player reward to rounded position.
        float f = Speedster.Distance / 10;
        f = Mathf.FloorToInt(f) * 10;
    }
}
