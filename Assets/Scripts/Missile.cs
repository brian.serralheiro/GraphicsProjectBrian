﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{

    private Rigidbody _rb;

	// Use this for initialization
	void Start ()
    {
        _rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
    {

        if (transform.position.z < -10)
        {
            //Randomly respawn in a lane in front of the player.
            transform.position = new Vector3(Random.Range(-1, 2), 0.5f, (transform.position.z * 5) / Speedster.Distance + transform.position.z + 150);
        }
    }

    void OnColissionEnter(Collider c)
    {
        if (c.gameObject.GetComponent<Speedster>())
        {
            c.gameObject.GetComponent<Speedster>().loselife();
        }
    }

    void FixedUpdate()
    {
        _rb.AddForce(0, 0, -10f);
    }
}
