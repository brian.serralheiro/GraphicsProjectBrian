﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacles : MonoBehaviour
{
    public GameObject coin; 

	// Update is called once per frame
	void Update ()
    {
        //Obstacles will move in the opposite direction of the player.
        transform.Translate(0, 0, -Speedster.SpeedPower * Time.deltaTime);
        if(transform.position.z < -10)
        {
            //Randomly respawn in a lane in front of the player.
            transform.position = new Vector3(Random.Range(-1, 2), 0.3f, (transform.position.z * 5) /Speedster.Distance + transform.position.z + 30);

            //Use raycast to detect if the obstacle is close to SuperJump(Tranpoline)
            RaycastHit hit;
            if(Physics.Raycast(transform.position, Vector3.forward, out hit, 5, 1 << 8 ))
            {
                transform.position = new Vector3(Random.Range(-1, 2), 0.3f, (transform.position.z * 5) / Speedster.Distance + transform.position.z + 30);
            }

            //50% of obstacles won't bring coins
            if (Random.value > 0.5f) return;
            for (int i = 0; coin && i < 3; i++)
            {
                Instantiate(coin, new Vector3(Random.Range(-1, 2),0.5f, transform.position.z + 3 + (i * 2)) , Quaternion.identity);
            }
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Speedster>())
        {
            other.gameObject.GetComponent<Speedster>().loselife();
        }
        if (other.gameObject.GetComponent<SuperJump>())
        {
            //provines obstacle spawning on top of an SuperJump object.
            transform.position = new Vector3(transform.position.x, transform.position.y, -10);
        }
    }
}
