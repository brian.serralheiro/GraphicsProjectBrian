﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GameData;
using System.IO;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
    {

    public Slider progress;
    public GameObject button1;
    public GameObject button2;
    public GameObject button3;

    public void Start()
    {
        Downloader.Init();
    }

    public void OnClick()
    {
       SceneManager.LoadScene("MyGame");
    }

    public void Instructions()
    {
        SceneManager.LoadScene("Instructions");
    }

    public void CreditsButton()
    {
        SceneManager.LoadScene("Credits");
    }
    public void Update()
    {
        if (!progress.IsActive()) return;

        progress.value = Downloader.Constant.progress + Downloader.Localidownload.progress;

        if(Downloader.Constant.isDone && Downloader.Localidownload.isDone)
        {
            if (string.IsNullOrEmpty(Downloader.Constant.text))
            {
                Debug.LogError("NO DATA WAS RECEIVED");
                //Load the last cached values

            }
            else
            {
                Debug.Log(Downloader.Localidownload.text);
                //Successfully got the data, process it

                //Save to disk

                //Save the file locally to the project's folder.
                File.WriteAllText(Constants.FilePath, Downloader.Constant.text);
                File.WriteAllText(Localization.FilePath, Downloader.Localidownload.text);   

            }
            progress.gameObject.SetActive(false);
            button1.SetActive(true);
            button2.SetActive(true);
            button3.SetActive(true);
        }

    }
}
