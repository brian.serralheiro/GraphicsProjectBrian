﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GameData; // To use the script Constants
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
public class Speedster : MonoBehaviour
{
	private const float MIN_SWIPE_LENGTH = 50f;
	private const float MAX_SWIPE_TIME = 0.35f;

	private Rigidbody _rb;
	private Vector3 _mouseStartPos;
	private float _elapsedTime;
	private bool _startTimer;

    public static float SpeedPower = 5;

    private static float MaxSpeed;

	public static bool _enableSwipe = true;

    public static int Lives;

    private static int Coins;

    private static float JumpForce;
    [SerializeField]
    private float JumpSpeed;

    private static int LivesValue;

    private static int MaxLives;

    public static float Distance;

    public Text DistanceUI;

    public Text CoinUI;

    public Text LivesUI;

    //Audio
    [SerializeField]
    private AudioClip JumpSFX;
    [SerializeField]
    private AudioSource audioSource;

	void Start()
	{
        Constants.Initialize();
        _rb = GetComponent<Rigidbody>();
		_mouseStartPos = new Vector3(0f, 0f, 0f);
		_elapsedTime = 0f;
        MaxSpeed = Constants.GetFloat("MaxSpeed");
        JumpForce = Constants.GetFloat("JumpForce");
        LivesValue = Constants.GetInt("LivesValue");
        MaxLives = Constants.GetInt("MaxLives");
        WallObstacles.WallGap = Constants.GetInt("WallGap");
        Lives = MaxLives;
        //Making a static instant being accesible
    }

	void Update()
	{
        if (JumpSpeed > 0.2f)
        {
            //Velocity of the rigidbody to zero
            _rb.velocity = Vector3.zero;
            JumpSpeed *= 0.9f;
            transform.Translate(0, JumpSpeed * Time.deltaTime, 0);
        }


        if (SpeedPower > MaxSpeed)
        {
            SpeedPower = MaxSpeed;
        }

        Distance += SpeedPower * Time.deltaTime;
        DistanceUI.text = "Distance: " + Mathf.RoundToInt(Distance);
        CoinUI.text = "Coins: " + Coins;
        LivesUI.text = "Lives: " + Lives;
        //if not enable, it will ignore any attempt of swiping
        if (!_enableSwipe)
        {
            return;
        }
#if UNITY_EDITOR
		SimulateMouseSwipe();
#endif

#if UNITY_ANDROID || UNITY_IOS
		if (Input.touchCount == 1)
		{
			Touch touch = Input.GetTouch(0);
			if (touch.phase == TouchPhase.Began)
			{
				_startTimer = true;
				_elapsedTime = 0f;
			}

			if (touch.phase == TouchPhase.Ended && _elapsedTime < MAX_SWIPE_TIME)
			{
                //Calculate position between left and right
				SpinCube(Mathf.Abs(touch.deltaPosition.x) / touch.deltaPosition.x);
			}
            
            //Detect when you release the finger and compare the movimentation = 0
            if (touch.phase == TouchPhase.Ended && touch.deltaPosition == Vector2.zero)
            {
                Jump(1);
            }
            
		}
#endif

        if (_startTimer)
		{
			if (_elapsedTime < MAX_SWIPE_TIME)
			{
				_elapsedTime += Time.deltaTime;
			}

			if (_elapsedTime >= MAX_SWIPE_TIME)
			{
				_startTimer = false;
			}
		}
	}

	/// <summary>
	/// This will simulate both directional and free swipe using the Left mouse button.
	/// </summary>
	private void SimulateMouseSwipe()
	{
		if (Input.GetMouseButtonDown(0))
		{
			_mouseStartPos = Input.mousePosition;
			_startTimer = true;
			_elapsedTime = 0f;
		}

		if (Input.GetMouseButtonUp(0) && _elapsedTime < MAX_SWIPE_TIME)
		{
			_startTimer = false;

			Vector3 mouseEndPos = Input.mousePosition;
			Vector3 direction = (mouseEndPos - _mouseStartPos);

			SpinCube(Mathf.Abs(direction.x) / direction.x);
		}

        if (Input.GetKeyUp(KeyCode.Space))
        {
            Jump(1);
        }

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            PauseMenu.PAUSE();
        }
    }

    private void SpinCube(float pDirection)
        {
        //Move player to the swipe direction.
        //provines an error to NAN exepction.
        if (!float.IsNaN(pDirection))transform.Translate(pDirection, 0, 0);
            if (transform.position.x > 1)
            {
                transform.position = new Vector3(1, transform.position.y, transform.position.z);
            }
            if (transform.position.x < -1)
            {
                transform.position = new Vector3(-1, transform.position.y, transform.position.z);
            }
        }

    public void Jump(float f)
    {
        if (transform.position.y < 0.6f)
        {
             playSound(JumpSFX);
            //Added velocity to the jump
            JumpSpeed = JumpForce * f;
        }
    }

    public void loselife()
    {
        Lives--;
        if (Lives <= 0)
        SceneManager.LoadScene("GameOver");
        else LifeMenu.PAUSE();
    }

    public static void gainCoin(int i)
    {
        Coins += i;
        if (Coins >= LivesValue)
        {
            Coins = 0;
            if (++Lives > MaxLives)
            {
                Lives = MaxLives;
            }
        }

    }

    void playSound(AudioClip clip, float volume = 1.0f)
    {
        audioSource.volume = volume;
        audioSource.clip = clip;
        audioSource.Play();
    }


}