﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]

public class Translator : MonoBehaviour
{
    public void OnEnable()
    {
        Localization.Initialize();
        Text t = GetComponent<Text>();
        if (t.text.Length == 1)
        {
            //Convert the number to text(of each language)
            t.text = Localization.Get(int.Parse (t.text));
            Debug.Log(t.text);
        }
    }

}
