﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundMove : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        //Obstacles will move in the opposite direction of the player.
        transform.Translate(0, 0, -Speedster.SpeedPower * Time.deltaTime, Space.World);
        if (transform.position.z < -10)
        {

            transform.position = new Vector3(0, 0, transform.position.z + 56.6f); 

        }
    }
}
