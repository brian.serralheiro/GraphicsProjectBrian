using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameData;

public static class Localization
{
    //The dictionary
    private static Dictionary<int, string> localizations;

    private static bool Started;

    public static string FilePath
    {
        get
        {
            string persistentPath = Application.persistentDataPath;
            string savePath = "/Localization";

            //Return folder path in the project
            return persistentPath+ savePath;
        }
    }

    public static void Initialize()
    {
        //It will maintain the initialization only once.
        if (Started) return;
        Started = true;
        localizations = new Dictionary<int, string>(); 
        string text = "";
        Debug.Log(Downloader.Localidownload);
        if (Downloader.Localidownload.isDone)
        {
            text = Downloader.Localidownload.text; 
            //Discard the downloaded files from memory
            Downloader.Localidownload.Dispose();
        }
        else
        {
            text = Resources.Load<TextAsset>("Localization").text;     //Load the CSV
        }

        //Get the system language

        //Default language to english
        //1 for english, 2 for portuguese, 3 for spanish
        int languageIndex = 1;

        for (var i = 0; i < 3; i++)
        {

            //Get the language index

        }
        //separate lines, each line one different language
        string[] packs = text.Split('\n');
        //Separate words on every ",", only the selected language 
        string[] lines = packs[languageIndex].Split(',');
        for (int i = 0; i < lines.Length; i++)
        {
            //Parse the csv into the dictionary
            //string[] cols = 

            localizations.Add(i, lines[i]);
        }
    }

    //Get the localized string
    public static string Get(int key)
    {
        return localizations[key];
    }
}