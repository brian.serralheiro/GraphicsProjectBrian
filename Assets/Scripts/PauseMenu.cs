﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    private static PauseMenu pauseMenu;
	// Use this for initialization
	void Start ()
    {
        pauseMenu = this;
        gameObject.SetActive(false);
	}

    public static void PAUSE()
    {
        Speedster._enableSwipe = false;
        pauseMenu.gameObject.SetActive(true);
    }

    public void PAUSEANDROID()
    {
        PAUSE();
    }

    public void OnEnable()
    {
        Time.timeScale = 0;
    }

    public void OnDisable()
    {
        Time.timeScale = 1;
    }

    public void Continue()
    {
        Speedster._enableSwipe = true;
        gameObject.SetActive(false);
    }

    public void Quit()
    {
        SceneManager.LoadScene("MyMainMenu");
    }
}
