﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : MonoBehaviour
{
    private Material mat;

	// Use this for initialization
	void Start ()
    {
        mat = GetComponent<Renderer>().material;
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Move the texture of the road, to make it look like that the player is moving
        mat.mainTextureOffset = new Vector2 (mat.mainTextureOffset.x, mat.mainTextureOffset.y + Speedster.SpeedPower * Time.deltaTime / 50);
	}
}
