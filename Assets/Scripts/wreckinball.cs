﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wreckinball : Obstacles
{
    private HingeJoint joint;
    private bool right; 

	// Use this for initialization
	void Start ()
    {
        joint = gameObject.GetComponent<HingeJoint>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        //Obstacles will move in the opposite direction of the player.
        transform.parent.Translate(0, 0, -Speedster.SpeedPower * Time.deltaTime);
        if (transform.parent.position.z < -10)
        {
            //Randomly respawn in a lane in front of the player.
            transform.parent.position = new Vector3(0, 4, (transform.position.z * 5) / Speedster.Distance + transform.position.z + 35);
        }


        if (transform.position.x > 1.2f && !right)
        {
            right = true;
            //Invert the joint force rotation
            JointMotor motor = joint.motor;
            motor.targetVelocity *= -1;
            joint.motor = motor;
        }
        if (transform.position.x < -1.2f && right)
        {
            right = false;
            //Invert the joint force rotation
            JointMotor motor = joint.motor;
            motor.targetVelocity *= -1;
            joint.motor = motor;
        }
    }

    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.GetComponent<Speedster>())
        {
            c.gameObject.GetComponent<Speedster>().loselife();
        }
    }
}
