﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace GameData
{
	public static class Downloader
    {
		//Get these from your own URL
		const string SPREADSHEET_ID = "18iLOIn_flEr505YgaeFRr0DTsSSmCho0xJuCVg6G2_I"; //after the d/
		const string LOCALIZATION_TAB_ID = "966901989"; //gid
		const string CONSTANTS_TAB_ID = "38699716"; //gid
        const string URL_FORMAT = "https://docs.google.com/spreadsheets/d/{0}/export?format=csv&id={0}&gid={1}";
        public static WWW Constant;
        public static WWW Localidownload;


        //Get url by using File->Download As->CSV
        //Then open download history in chrome/firefox and copy url
        //Make sure accessiblity is set to Anyone with link can view

        public static void Init()
        {
			Localidownload = DownloadCSV(LOCALIZATION_TAB_ID);
			Constant = DownloadCSV(CONSTANTS_TAB_ID);
		}

		private static WWW DownloadCSV(string tabID)
        {

			//Get the formatted URL
			string downloadUrl = string.Format(URL_FORMAT, SPREADSHEET_ID, tabID);

			Debug.LogFormat("Downloading {0}" , downloadUrl);

			//Download the data
			WWW website = new WWW(downloadUrl);

            return website;
		}
	}
}
