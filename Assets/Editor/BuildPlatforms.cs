﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BuildPlatforms : Editor
{
    private static string[] scenes = new string[] 
    {
        "Assets/Scenes/MyMainMenu.unity",
        "Assets/Scenes/MyGame.unity",
        "Assets/Scenes/GameOver.unity",
        "Assets/Scenes/Instructions.unity",
        "Assets/Scenes/Credits.unity"

    };

    #region MENU_ITEMS
    [MenuItem("Build/PC/Dev")]
    public static void BuildPCDev()
    {
        Build(true, BuildTarget.StandaloneWindows);
    }
    [MenuItem("Build/PC/Rel")]
    public static void BuildPCRelease()
    {
        Build(false, BuildTarget.StandaloneWindows);
    }
    [MenuItem("Build/PC/All")]
    public static void BuildPCAll()
    {
        BuildPCDev();
        BuildPCRelease();
    }

    [MenuItem("Build/Android/Dev")]
    public static void BuildAndroidDev()
    {
        Build(true, BuildTarget.Android);
    }
    [MenuItem("Build/Android/Rel")]
    public static void BuildAndroidRelease()
    {
        Build(false, BuildTarget.Android);
    }
    [MenuItem("Build/Android/All")]
    public static void BuildAndroidAll()
    {
        BuildAndroidDev();
        BuildAndroidRelease();
    }
    [MenuItem("Build/All")]
    public static void BuildAll()
    {
        BuildAndroidAll();
        BuildPCAll();
    }
    #endregion

    public static string GetProjectPath()
    {
        string pathToProject = Application.dataPath;
        pathToProject = pathToProject.Replace("/Assets", "");

        return pathToProject;
    }

    public static void Build(bool isDev, BuildTarget target)
    {
        BuildPlayerOptions options = new BuildPlayerOptions();
        string extension = string.Empty;

        switch (target)
        {
            case BuildTarget.StandaloneWindows:
                extension = ".exe";
                break;
            case BuildTarget.Android:
                extension = ".apk";
                break;
        }

        options.locationPathName = string.Format("{0}/Build/{1}/Speedster_1.0.0_{1}{2}{3}", GetProjectPath(), target, isDev ? "_Dev" : "", extension);
        options.scenes = scenes;
        options.target = target;
        options.options = isDev ? BuildOptions.Development : BuildOptions.None;

        Debug.LogFormat("Building {0} to {1}\nDev: {2}", options.target, options.locationPathName, isDev);
        BuildPipeline.BuildPlayer(options);
    }

}
